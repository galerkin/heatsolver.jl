module HeatSolver

using FFTW
using MatrixDepot
using DifferentialEquations
using LinearAlgebra

export ring1D
export bar1D
export bar1D_steady
export heat_operator
export subdiv_len
export laplacian1D_order2

@enum SolutionMethod Spectral FinDiff

"""
    ring1D(α, T, N, algo)

Solve the heat equation around a circle. 
Here α is the thermal diffusivity[1], T is the time, f0 is a function that
gives us the initial heat distribution, and N is the number of points used in
the discrete approximation.

[1] https://en.wikipedia.org/wiki/Thermal_diffusivity
"""
function ring1D(α, T, f_0::Function, N, algo::SolutionMethod)
  tol = 1e-9
  @assert ((abs(f_0(0)) < tol) && (abs(f_0(2π)) < tol)) "Due to my own ad-hoc
  assumptions, f_0 must be periodic and zero at x=0!"
  
  if algo == Spectral  # use N-point DFT
    @assert iseven(N) "Expected N to be even!"

    greens = λ -> exp(-α*λ^2*T) # Green's function for this problem

    # domain is defined on interval [a, b]
    a = 0.0
    b = 2π
    x = LinRange(a, b, N)
    
    # We only consider N/2 + 1 points in Fourier space because we use rfft()
    # instead of fft()
    k = k = 0:(N/2) #discrete "index", starting at 0 by convention for DFTs
    u_0 = f_0.(x) # initial distribution
    u_0_hat = rfft(u_0)
    u_T_hat = u_0_hat.*greens.(k)
    return irfft(u_T_hat, N)
  else 
    throw(ArgumentError("Solution method has not yet been implemented"))
  end
end


function laplacian1D_order2(h,N)
  return (1.0/h^2)*matrixdepot("tridiag",N) 
end

function heat_operator(α,L,N)
 return -α*laplacian1D_order2(subdiv_len(L,N), N) 
end

"Compute the length (h) of a subdivision, given N interior points"
function subdiv_len(dim_len, N)
  # there are n+1 intervals, see Fig 2. of CS6220 handout 2
  return dim_len/(N+1) 
end

"""
    bar1D(A, L, N, u_0, t_0, t_final, algo)

    Solve the heat equation on a unit bar where A is the heat matrix (matrix
    version of the negative laplacian times thermal diffusivity),  L is
    the length of the bar, u_0 is the initial heat distribution, t_0 is the
    initial time, t_final is the final time, and algo is the SolutionMethod
    used.

    When using periodic boundary conditions, N is the number of points in the
    domain (there are no endpoints, since the domain is effectively a ring).
    Otherwise, N is the number of *interior* points (i.e. NOT including
    left/right endpoints),

    Returns a struct that represents the output of the ODE solver
"""
function bar1D(A, L, N, u_0::Array{Float64,1}, t_0, t_final, algo::SolutionMethod)

  @assert size(u_0,1) == N "Size of u_0 must match number of points (interior points if using non-periodic BCs!"

  if algo == FinDiff
    # A = laplacian1D_order2(subdiv_len(L,N), N)
    tspan = (t_0, t_final)
    # A*u computes the negative of the second derivative, see p.5 of Handout 2
    # from CS6220
    # f(u,p,t) = -α*A*u # default (NOT in-place) function
    f(du,u,p,t) = mul!(du,A,u) # "in-place" solve. It should incur less GC overhead
    ode_prob = ODEProblem(f,u_0,tspan)

    ode_sln = solve(ode_prob, Tsit5())
    # ode_sln = solve(ode_prob)
    return ode_sln 
  else 
    throw(ArgumentError("Solution method has not yet been implemented"))
  end

end

"""
    bar1D_steady(α, L, N, algo)

    Solve the Laplace equation on a unit bar where α is the [thermal
    diffusivity](https://en.wikipedia.org/wiki/Thermal_diffusivity), N is the
    number of *interior* points in the discretization (i.e. not counting the
    left/right endpoints), dirichletBC1 and dirichletBC2 are the prescribed
    temperature of the left/right endpoints, and algo is the SolutionMethod
    used.

"""
function bar1D_steady(α, L, N, dirichletBC1, dirichletBC2, algo::SolutionMethod)

  function laplacian1D_order2_dirichlet(α, h, n)
    A = α*laplacian1D_order2(h,n)
    # Set thes values in order to enforce BCs
    A[1,1] = 1.0
    A[1,2] = 0.0
    A[n,n] = 1.0
    A[n,n-1] = 0.0
    return A
  end

  if algo == FinDiff
    # Here, the size of M is (N+2)x(N+2), because we use the 
    # first and last rows to enforce the Dirichlet BCs.
    M = laplacian1D_order2_dirichlet(α, subdiv_len(L,N), N+2)
    rhs = [dirichletBC1; zeros(N); dirichletBC2]
    return M\rhs
  else 
    throw(ArgumentError("Solution method has not yet been implemented"))
  end
end

end # module
